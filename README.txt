This module integrates the <a href="http://ace.c9.io/#nav=about">Ajax Cloud9
Editor</a>(ACE) with the <a href="https://drupal.org/project/wysiwyg">WYSIWYG</a>
module.

The initial code is taken from patch #27 in [#1377948]. If the patch becomes
part of WYSIWYG, this module becomes deprecated. Until such time: this is a
nice way of getting the functionality without having to patch WYSIWYG.

INSTALLATION

<ol>
  <li>Install the WYSIWYG and WYSIWYG Ace Editor modules</li>
  <li>Follow the instructions in the WYSIWYG module. Get the JS libraries from
  <a href="https://github.com/ajaxorg/ace-builds/">Github</a> and place them in
  the designated <code>libraries</code> folder</li>
  <li>Create a WYSIWYG profile and associate the editor with a text format</li>
  <li>Profit!</li>
</ol>

CREDITS

The original code for this module was written by these developers and can be
found in issue #1377948 https://drupal.org/node/1377948

- mikeprinsloo (https://drupal.org/user/98684)
- TravisCarden (https://drupal.org/user/236758)
- mstrelan (https://drupal.org/user/314289)
